import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';

interface States {
  name: string,
  age?: number
}

interface Props {
  isStudent?: boolean
}

const SubText: React.FC<Props> = (props) => {
  return (
      <div>
          {
              (props.isStudent)
                  ? <p>You are a student.</p>
                  : <p>You aren't a student.</p>
          }
      </div>
  );
};

function App() {
  const [user, setUser] = useState<States>({name:'van', age:15});
  const name: string = 'aaa'
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <h1>{user.name}</h1>
        <h1>{name}</h1>
        <SubText isStudent={false} />
      </header>
    </div>
  );
}

export default App;
